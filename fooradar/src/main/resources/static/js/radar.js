 /**
  * Radar screen implementation (phaser engine)
  * @author Lukas Boleslavsky
  * Date: 18.5.2018
  */  
window.onload = function() {

        var game = new Phaser.Game(800, 600, Phaser.AUTO, 'radar-display');
        var that;
       
        var FlightRadarCore = function(){
            this.FLIGHT_SERVER_PERIOD = 2500;
            this.markups = [];
            that = this;
        }   

        FlightRadarCore.prototype.preload = function(){
            game.load.image('dot', 'dot1.png');
            game.load.image('dir', 'dir.png');
            game.load.image('labelbg', 'labelbg.png');
            
            game.load.start();
        }

        FlightRadarCore.prototype.create = function(){
            game.physics.startSystem(Phaser.Physics.ARCADE);
     
            this.getRadarSnapshot();
           
            // call flight server periodically
            this.timer = game.time.events.loop(this.FLIGHT_SERVER_PERIOD, this.getRadarSnapshot.bind(this));
        }
        
        FlightRadarCore.prototype.update = function(){
            if(this.markups==undefined){
                return;
            }
        
            // draw lines between markup and text label
            for(var i=0;i<this.markups.length;i++){                
                this.markups[i].line.fromSprite(this.markups[i].t, this.markups[i].label);
                game.debug.geom(this.markups[i].line);
            }
        }
        
        /**
         * Update flight item position
         * 
         * @param {type} json
         * @returns {undefined}
         */
        FlightRadarCore.prototype.updatePosition = function(json){
            
            var id;
            for(var i=0;i<json.flightItems.length;i++){
                
                id = json.flightItems[i].id;
                
                if(this.markups[id]==undefined){
                    this.addMarkup(json.flightItems[i]);
                } else {
                    this.markups[id].text.setText(this.getTelemetryText(json.flightItems[i]));
                }
                
                if(json.flightItems[i].onRadar){            
                    this.markups[id].label.x+= (json.flightItems[i].x*3)-this.markups[id].t.x;
                    this.markups[id].label.y+= (json.flightItems[i].y*3)-this.markups[id].t.y;
                
                    this.markups[id].t.x = json.flightItems[i].x*3;
                    this.markups[id].t.y = json.flightItems[i].y*3;
                    
                    this.markups[id].dir.x = this.markups[id].t.x;
                    this.markups[id].dir.y = this.markups[id].t.y;
                    this.markups[id].dir.angle = json.flightItems[i].direction;
                    
                } else {               
                    this.markups[id].t.alpha=0.5;
                }
            }                              
        }
        
        /**
         * Create new markup
         * 
         * @param {type} flightItem flight item
         */
        FlightRadarCore.prototype.addMarkup = function(flightItem){
            if(flightItem==undefined){
                return;
            }
            
            var t = game.add.sprite(flightItem.x,flightItem.y,'dot');
            var dir = game.add.sprite(flightItem.x,flightItem.y,'dir');
            var label = game.add.sprite(flightItem.x+50, flightItem.y+50, 'labelbg');
            var text =  game.add.text(0,0,this.getTelemetryText(flightItem),{ font: "15px Arial", fill: "#ffffff", align: "left" });
            var line = new Phaser.Line(t.x, t.y, label.x, label.y);
            
            label.anchor.setTo(0.5);
            t.anchor.setTo(0.5);
            dir.anchor.setTo(0,0.5);
            dir.angle = flightItem.direction;          
            
            label.addChild(text);
            
            game.physics.enable([t, label], Phaser.Physics.ARCADE);
            
            label.inputEnabled=true;
            label.input.enableDrag(true);           
            
            this.markups[flightItem.id] = {t: t, dir:dir, label: label, text:text, line:line, flightItem: flightItem};     
        }
        
        FlightRadarCore.prototype.getTelemetryText=function(flightItem){
            return flightItem.name+ '\n' + flightItem.direction + ' ' + this.getFL(flightItem.altitude) + '\n' + flightItem.speed;
        }

        /**
         * Call flight server
         */
        FlightRadarCore.prototype.getRadarSnapshot = function(){
        
            $.getJSON('radar')
            .done(function( json ) {
                that.updatePosition(json);        
            })
            .fail(function( jqxhr, textStatus, error ) {
              var err = textStatus + ", " + error;
              console.log( "Request Failed: " + err );
          });
        }
        
        FlightRadarCore.prototype.getFL = function(altitude){
            return 'FL' + Math.round((altitude*3.281)/100);
        }
            
        // prepare and start client
        var flightRadarCore = new FlightRadarCore();
        
        game.state.add('flightRadarCore', flightRadarCore );
        game.state.start('flightRadarCore');    
    };