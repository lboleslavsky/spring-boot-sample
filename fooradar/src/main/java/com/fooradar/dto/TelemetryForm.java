package com.fooradar.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * Form for telemetry change
 * TODO: implement custom validator?
 * 
 * @author Lukas Boleslavsky
 * Date: 17.5.2018
 */
public class TelemetryForm {
    private int id;
    
    @Min(0)
    @Max(360)
    private int direction;
    
    @Min(0)
    @Max(5000)
    private int speed;

    @Min(1000)
    @Max(20000)
    private int altitude;
    
    /**
     * Constructor
     */
    public TelemetryForm(){
    }
    
    /**
     * Constructor
     * 
     * @param direction flight item direction
     * @param speed flight item speed
     * @param altitude flight item altitude
     */
    public TelemetryForm(int direction, int speed, int altitude){
        this.direction= direction;
        this.speed = speed;
        this.altitude = altitude;       
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the direction
     */
    public int getDirection() {
        return direction;
    }

    /**
     * @return the speed
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * @return the altitude
     */
    public int getAltitude() {
        return altitude;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @param direction the direction to set
     */
    public void setDirection(int direction) {
        this.direction = direction;
    }

    /**
     * @param speed the speed to set
     */
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    /**
     * @param altitude the altitude to set
     */
    public void setAltitude(int altitude) {
        this.altitude = altitude;
    }
}
