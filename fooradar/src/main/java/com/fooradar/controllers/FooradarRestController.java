package com.fooradar.controllers;

import com.fooradar.service.RadarService;
import com.fooradar.exception.FlightItemTelemetryChangeException;
import com.fooradar.dto.RadarSnapshot;
import com.fooradar.dto.TelemetryForm;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Radar REST controller API
 * 
 * @author Lukas Boleslavsky
 * Date: 17.5.2018
 */
@RestController
public class FooradarRestController {
    
    private final RadarService radarService; 
    
    @Autowired
    public FooradarRestController(RadarService radarService){
        this.radarService = radarService;
    }
    
    @RequestMapping(path = "/radar", method = RequestMethod.GET)
    public RadarSnapshot getFlightItems() {
        return radarService.getRadarSnapshot();
    }    
      
    @RequestMapping(path="/telemetryJson", method = RequestMethod.POST)
    public ResponseEntity<?> setTelemetryJson(@Valid @RequestBody TelemetryForm form){
        
        try{
            radarService.setTelemetry(form.getId(), form.getDirection(), form.getSpeed(), form.getAltitude());
        } catch (Exception ex){
            throw new FlightItemTelemetryChangeException("Error during changing telemetry.");
        }
        
        return ResponseEntity.ok().build();
    }
}
