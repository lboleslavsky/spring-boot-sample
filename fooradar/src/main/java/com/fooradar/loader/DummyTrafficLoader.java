package com.fooradar.loader;

import com.fooradar.dto.FlightItem;
import java.util.ArrayList;
import java.util.List;

/**
 * Dummy data loader
 * 
 * @author Lukas Boleslavsky
 * Date: 18.5.2018
 */
public class DummyTrafficLoader implements TrafficLoader {

    @Override
    public List<FlightItem> load() {
        //TODO: load from some config file... 
        List<FlightItem> items = new ArrayList<FlightItem>();
        
        items.add(new FlightItem("OK-125", 0, 2000, 10000, 45, 250, 3048));
        items.add(new FlightItem("KL-501", 1, 50000, 10000, 145, 500, 6096));
        items.add(new FlightItem("WK-295", 2, 60000, 50000, 260, 350, 3048));
        items.add(new FlightItem("KR-102", 3, 60000, 15000, 260, 350, 6096));
        items.add(new FlightItem("KL-820", 4, 3500, 45000, 320, 250, 3048));
        return items;
    }    
}
