package com.fooradar.dto;

/**
 * Flight item data object
 *
 * @author Lukas Boleslavsky
 * Date: 17.5.2018
 */
public class FlightItem {

    /**
     * Constructor
     *
     * @param name flight item name
     * @param id identifier
     * @param latitude world pos latitude in meters
     * @param longtitude world pos longtitude in meters
     * @param direction flight item direction angle in degrees
     * @param speed flight item speed in kph
     * @param altitude flight item altitude in meters
     */
    public FlightItem(String name, long id, int latitude, int longtitude, float direction, int speed, int altitude) {
        this.name = name;
        this.id = id;
        this.latitude = latitude;
        this.longtitude = longtitude;
        this.direction = direction;
        this.speed = speed;
        this.altitude = altitude;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param x the pixel x coordinate to set
     */
    public void setX(float x) {
        this.x = x;
    }

    /**
     * @return x the pixel x coordinate
     */
    public float getX() {
        return x;
    }

    /**
     * @param y the pixel y coordinate to set
     */
    public void setY(float y) {
        this.y = y;
    }

    /**
     * @return the pixel y coordinate
     */
    public float getY() {
        return y;
    }

    /**
     * @return the direction
     */
    public float getDirection() {
        return direction;
    }

    /**
     * @param direction the direction to set
     */
    public void setDirection(int direction) {
        this.direction = direction;
    }

    /**
     * @return the speed
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * @param speed the speed to set
     */
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    /**
     * @return the altitude
     */
    public int getAltitude() {
        return altitude;
    }

    /**
     * @param altitude the altitude to set
     */
    public void setAltitude(int altitude) {
        this.altitude = altitude;
    }

    /**
     * @return the targetDirection
     */
    public int getTargetDirection() {
        return targetDirection;
    }

    /**
     * @param targetDirection the targetDirection to set
     */
    public void setTargetDirection(int targetDirection) {
        this.targetDirection = targetDirection;
    }

    /**
     * @return the deltaDirection
     */
    public int getDeltaDirection() {
        return deltaDirection;
    }

    /**
     * @param deltaDirection the deltaDirection to set
     */
    public void setDeltaDirection(int deltaDirection) {
        this.deltaDirection = deltaDirection;
    }

    /**
     * @return the targetSpeed
     */
    public int getTargetSpeed() {
        return targetSpeed;
    }

    /**
     * @param targetSpeed the targetSpeed to set
     */
    public void setTargetSpeed(int targetSpeed) {
        this.targetSpeed = targetSpeed;
    }

    /**
     * @return the deltaSpeed
     */
    public int getDeltaSpeed() {
        return deltaSpeed;
    }

    /**
     * @param deltaSpeed the deltaSpeed to set
     */
    public void setDeltaSpeed(int deltaSpeed) {
        this.deltaSpeed = deltaSpeed;
    }

    /**
     * @return the targetAltitude
     */
    public int getTargetAltitude() {
        return targetAltitude;
    }

    /**
     * @param targetAltitude the targetAltitude to set
     */
    public void setTargetAltitude(int targetAltitude) {
        this.targetAltitude = targetAltitude;
    }

    /**
     * @return the deltaAltitude
     */
    public int getDeltaAltitude() {
        return deltaAltitude;
    }

    /**
     * @param deltaAltitude the deltaAltitude to set
     */
    public void setDeltaAltitude(int deltaAltitude) {
        this.deltaAltitude = deltaAltitude;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the latitude
     */
    public long getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(long latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longtitude
     */
    public long getLongtitude() {
        return longtitude;
    }

    /**
     * @param longtitude the longtitude to set
     */
    public void setLongtitude(long longtitude) {
        this.longtitude = longtitude;
    }

    /**
     * @return the onRadar
     */
    public boolean isOnRadar() {
        return onRadar;
    }

    /**
     * @param onRadar the onRadar to set
     */
    public void setOnRadar(boolean onRadar) {
        this.onRadar = onRadar;
    }

    private final long id;
    private final String name;
    private long latitude;
    private long longtitude;
    private float x;
    private float y;
    private float direction;
    private int targetDirection;
    private int deltaDirection;
    private int speed;
    private int targetSpeed;
    private int deltaSpeed;
    private int altitude;
    private int targetAltitude;
    private int deltaAltitude;

    private boolean onRadar;
}
