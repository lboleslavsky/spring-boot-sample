function resetForm(){
    resetValue('direction');
    resetValue('speed');
    resetValue('altitude');
}

function sendJson(){   
  
    var jsonObj = {id: parseValue('id'), direction:parseValue('direction'), speed:parseValue('speed'), altitude:parseValue('altitude')};
        
    $.ajax({ 
        url:'telemetryJson',
        type:"POST", 
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(jsonObj),
        async: false,   
        cache: false,   
        processData:false,
            success: function(resposeJsonObject){
                //TODO: on success 
                $('#changeTelemetryModal').modal('toggle');
            }, 
            
            error: function(err){
                alert('Please enter valid telemetry data.');
            } 
    });                 
                
    return true;
}
            
function parseValue(name){
    return document.getElementById(name).value;
}    

function resetValue(name){
    document.getElementById(name).value='';
}
