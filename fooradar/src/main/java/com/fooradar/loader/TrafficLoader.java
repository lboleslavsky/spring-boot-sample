package com.fooradar.loader;

import com.fooradar.dto.FlightItem;
import java.util.List;

/**
 * Traffic loader 
 * 
 * @author Lukas Boleslavsky
 */
public interface TrafficLoader {
    List<FlightItem> load();
}
