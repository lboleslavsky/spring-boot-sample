package com.fooradar;

import com.fooradar.service.RadarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Timer component for calling RadarService (periodically)
 *
 * @author Lukas Boleslavsky
 * Date: 17.5.2018
 */
@Component
public class RadarUpdateTask {

    private final RadarService radarService;

    /**
     * Constructor
     *
     * @param radarService
     */
    @Autowired
    public RadarUpdateTask(RadarService radarService) {
        this.radarService = radarService;
    }

    /**
     * Calling radar service every 5000 ms (refreshing)
     */
    @Scheduled(fixedRate = 5000)
    public void callRadarService() {
        radarService.updatePositions();
    }
}
