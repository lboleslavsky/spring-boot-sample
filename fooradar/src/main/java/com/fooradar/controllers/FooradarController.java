package com.fooradar.controllers;

import com.fooradar.dto.Callsign;
import com.fooradar.dto.FlightItem;
import com.fooradar.service.RadarService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Main controller
 * 
 * @author Lukas Boleslavsky
 * Date: 17.5.2018
 */
@Controller
public class FooradarController {
    private final RadarService radarService; 
    
    @Autowired
    public FooradarController(RadarService radarService){
        this.radarService = radarService;
    }
    
    @RequestMapping("/")
    public String welcome(Map<String, Object> model) {
	
        List<FlightItem> items = this.radarService.getRadarSnapshot().getFlightItems();
        List<Callsign> callsigns = new ArrayList<Callsign>();
        
        for(FlightItem item: items){
            callsigns.add(new Callsign(item.getName(),item.getId()));
        }

        Collections.sort(callsigns, new Comparator<Callsign>(){
            @Override
            public int compare(Callsign c1, Callsign c2){
                return c1.getName().compareToIgnoreCase(c2.getName());
            }        
        });
        
        model.put("callsigns", callsigns);
        return "index";
    }
}
