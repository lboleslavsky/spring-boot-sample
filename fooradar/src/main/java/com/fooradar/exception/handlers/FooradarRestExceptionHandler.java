package com.fooradar.exception.handlers;

import com.fooradar.dto.ErrorMessage;
import com.fooradar.exception.FlightItemTelemetryChangeException;
import java.util.Date;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Handling custom Exceptions
 * 
 * @author Lukas Boleslavsky
 * Date: 17.5.2018
 */
@ControllerAdvice
@RestController
public class FooradarRestExceptionHandler extends ResponseEntityExceptionHandler {
  
   @ExceptionHandler(FlightItemTelemetryChangeException.class)
    public final ResponseEntity<Object> handleException(FlightItemTelemetryChangeException ex, WebRequest request) {
        ErrorMessage errorDetails = new ErrorMessage(new Date(), ex.getMessage(),
            request.getDescription(false));
        return new ResponseEntity(errorDetails, HttpStatus.NOT_FOUND);
    }
    
    /**
     * Overriding handler for exceptions during validation
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {

        ErrorMessage errorDetails = new ErrorMessage(new Date(), ex.getMessage(),
            request.getDescription(false));
     
        return new ResponseEntity(errorDetails, HttpStatus.BAD_REQUEST);
    }
}
