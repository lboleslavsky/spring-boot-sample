package com.fooradar.service;

import com.fooradar.dto.FlightItem;
import com.fooradar.dto.RadarSnapshot;
import java.util.List;

/**
 * Radar service implementation (dummy simulation).
 *
 * @author Lukas Boleslavsky
 * Date: 17.5.2018
 */
public class FlightRadarService implements RadarService {

    /**
     * Constructor
     *
     * @param flightItems collection of flight items
     */
    public FlightRadarService(List<FlightItem> flightItems) {
        this.flightItems = flightItems;
    }

    /**
     * Return info about this service
     *
     * @return String info text
     */
    @Override
    public String getInfo() {
        return INFO;
    }

    /**
     * Return radar snapshot of current moment
     *
     * @return radar snapshot with flight items
     */
    @Override
    public RadarSnapshot getRadarSnapshot() {
        return new RadarSnapshot(System.currentTimeMillis(), flightItems);
    }

    /**
     * Recalculate and update position of every detected flight item
     */
    @Override
    public void updatePositions() {

        float distance;
        for (FlightItem flightItem : flightItems) {

            // Calculating traveled distance
            distance = getDistance(flightItem.getSpeed(), 5);

            // Obtaining next point of current direction. We know current point, angle and distance  
            flightItem.setLatitude(flightItem.getLatitude() + (int) (Math.cos(Math.toRadians(flightItem.getDirection())) * distance));
            flightItem.setLongtitude(flightItem.getLongtitude() + (int) (Math.sin(Math.toRadians(flightItem.getDirection())) * distance));

            // Converting word coordinates to pixel point depends on convert scale ratio
            flightItem.setX(flightItem.getLatitude() / MAP_SCALE_X);
            flightItem.setY(flightItem.getLongtitude() / MAP_SCALE_Y);

            // Detecting collisions
            flightItem.setOnRadar(!isCollision(flightItem));
        }
    }

    /**
     * Setting new telemetry of flight item (adressed with id)
     *
     * @param id adressed flight item
     * @param direction direction angle in degrees
     * @param speed speed in kph
     * @param altitude altitude in meters
     */
    @Override
    public void setTelemetry(int id, int direction, int speed, int altitude) {
        FlightItem flightItem = flightItems.get(id);

        flightItem.setDirection(direction);
        flightItem.setSpeed(speed);
        flightItem.setAltitude(altitude);
    }

    /**
     * Calculate if there is any collision with other flight item
     *
     * @param current current flight item
     * @return true if there is any collision
     */
    private boolean isCollision(FlightItem current) {

        boolean isCollision = false;
        for (FlightItem flightItem : flightItems) {

            /*
                This is very simple collision test. Note that one pixel point [getX(), getY()] here depends on used map scale [getLatitude(), getLongtitude()].  
                TODO: Maybe there should be some propability of collision (derived from both flight item types). 
                But for example just testing if position and altitude of both flight items is the same. 
             */
            if (current != flightItem && (current.getX() == flightItem.getX() && current.getY() == flightItem.getY() && current.getAltitude() == flightItem.getAltitude())) {
                flightItem.setOnRadar(false);

                // No, because there can be more collisions: 
                // return true;
                isCollision = true;
            }
        }
        return isCollision;
    }

    /**
     * Calculate traveled distance
     *
     * @param velocity speed in kph
     * @param time time in seconds
     * @return traveled distance
     */
    private float getDistance(float velocity, int time) {
        return (velocity / SPEED_CONVERT_RATIO) * time;
    }
    
    // Collection of all detected items
    private List<FlightItem> flightItems;
    
    // Constants
    private final String INFO = "Dummy flight radar.";
    private final float SPEED_CONVERT_RATIO = 3.6f;
    private final short MAP_SCALE_X = 300;
    private final short MAP_SCALE_Y = 300;
}
