package com.fooradar;

import com.fooradar.loader.DummyTrafficLoader;
import com.fooradar.service.RadarService;
import com.fooradar.service.FlightRadarService;
import java.util.List;
import com.fooradar.dto.FlightItem;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Application starter
 *
 * @author Lukas Boleslavsky
 * Date: 17.5.2018
 */
@SpringBootApplication
@EnableScheduling
public class FooradarApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(FooradarApplication.class, args);
    }

    /**
     * Create RadarService bean
     *
     * @return radar service bean
     */
    @Bean
    public RadarService getRadarService() {
        return new FlightRadarService(getInit());
    }

    /**
     * Create flight items
     *
     * @return flight items
     */
    private List<FlightItem> getInit() {       
       return new DummyTrafficLoader().load();
    }
}
