package com.fooradar.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Flight item not found Exception
 *
 * @author Lukas Boleslavsky
 * Date: 17.5.2018
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class FlightItemTelemetryChangeException extends RuntimeException {

    /**
     * Constructor
     *
     * @param message error message
     */
    public FlightItemTelemetryChangeException(String message) {
        super(message);
    }
}
