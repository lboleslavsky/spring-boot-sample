package com.fooradar.dto;

import java.util.List;

/**
 * Radar snapshot data object
 * 
 * @author Lukas Boleslavsky
 * Date: 17.5.2018
 */
public class RadarSnapshot {  
    
    /**
     * Constructor
     * 
     * @param ts current timestamp
     * @param flightItems 
     */
    public RadarSnapshot(long ts, List<FlightItem> flightItems){
        this.ts = ts;
        this.flightItems = flightItems;
    }

    /**
     * @return the current timestamp 
     */
    public long getTs() {
        return ts;
    }

    /**
     * @return the flightItems
     */
    public List<FlightItem> getFlightItems() {
        return flightItems;
    }
    
    private final long ts;
    private final List<FlightItem> flightItems;    
}
