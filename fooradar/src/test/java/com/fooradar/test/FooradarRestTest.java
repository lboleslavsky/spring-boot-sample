package com.fooradar.test;

import io.restassured.RestAssured;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import org.junit.Before;

/**
 * Test class
 * 
 * @author Lukas Boleslavsky
 * Date: 17.5.2018
 */
@SpringBootTest
public class FooradarRestTest extends AbstractTestNGSpringContextTests {
    
    @Before
    public void setup() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 8080;
    }    
    
    /**
     * Testing value presence in radar data
     */
    public void first(){        
        when().get("/radar").then().body("flightItems.direction", hasItems(45.0f));
        
        when().get("/radar").then().assertThat().body("flightItems[id=0].direction", equalTo(45.0f)).and().body("flightItems[id=0].speed", equalTo(250));
    }
}
