package com.fooradar.service;

import com.fooradar.dto.RadarSnapshot;

/**
 * Radar service interface.
 *
 * @author Lukas Boleslavsky
 * Date: 17.5.2018
 */
public interface RadarService {

    /**
     * Return info about this service
     *
     * @return String info text
     */
    String getInfo();

    /**
     * Return radar snapshot of current moment
     *
     * @return radar snapshot with flight items
     */
    RadarSnapshot getRadarSnapshot();

    /**
     * Recalculate and update position of every detected flight item
     */
    void updatePositions();

    /**
     * Setting new telemetry of flight item (adressed with id)
     *
     * @param id adressed flight item
     * @param direction direction angle in degrees
     * @param speed speed in kph
     * @param altitude altitude in meters
     */
    void setTelemetry(int id, int direction, int speed, int altitude);
}
