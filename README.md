![Fooradar screenshot](/source/images/radar.png "Fooradar")

## Not implemented yet (?)

1. What if plane is out of bounds
2. Smoother plane altitude, speed and direction change (pilot needs some time to proceed)
3. Add some chat with pilots
4. Collision warnings to radar screen
5. Flight item trajectory history

