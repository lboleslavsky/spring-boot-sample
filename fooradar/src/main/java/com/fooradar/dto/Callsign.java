package com.fooradar.dto;

/**
 * Callsign object
 * 
 * @author Lukas Boleslavsky
 * Date: 18.5.2018
 */
public class Callsign {

    private String name;
    private long id;
    
    public Callsign(String name, long id){  
        this.name=name;
        this.id=id;
    }
    
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }    
}
